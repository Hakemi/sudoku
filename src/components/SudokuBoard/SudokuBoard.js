import React from 'react';
import { sqr, solvedSelected } from '../../utils/constants';


function getBackroundColor(index, backgroundColor, selectedItem) {
  if (backgroundColor && selectedItem === index) {
    return solvedSelected;
  }
  if (backgroundColor) {
    return backgroundColor;
  }

  if (selectedItem === index) {
    return 'gray';
  }

  return 'white';
}

export default function SudokuBoard({ selectedItem, elements, itemSelectHandler }) {
  return (
    <div className="grid-container">
      {elements.map((v) => (
        <button
          key={v.index}
          style={{
            borderBottomWidth: Number.isInteger(
              (v.row + 1) / sqr,
            )
              ? 5
              : 1,
            borderRightWidth: Number.isInteger((v.column + 1) / sqr)
              ? 5
              : 1,
            borderLeftWidth: v.column === 0 ? 5 : 1,
            borderTopWidth: v.row === 0 ? 5 : 1,
            backgroundColor: getBackroundColor(v.index, v.backgroundColor, selectedItem),
          }}
          className="grid-item"
          onClick={() => !v.show && itemSelectHandler(v.index)}
        >
          <p style={{ opacity: v.value ? 1 : 0, color: v.color }}>
            {v.value || 0}
          </p>
        </button>
      ))}

    </div>
  );
}
