import React from 'react';
import { orderBy } from 'lodash';
import {
  entryRange, EASY, MEDUIM, HARD,
} from '../../utils/constants';
import { generateSudoku } from '../../utils/handlers';


export default function ({
  keypadClickHandler, setElements, level, handleSolveAll, setLevel,
}) {
  return (
    <div className="kepad-container">
      <div className="grid-container-keypad">
        {orderBy(entryRange).map((v) => (
          <button key={v} className="grid-item-keypad" onClick={() => keypadClickHandler(v)}>
            {v}
          </button>
        ))}
      </div>
      <select
        value={level}
        className="select"
        onChange={(e) => {
          const { value } = e.target;
          const newLevel = Number(value);
          setElements(generateSudoku(newLevel));
          setLevel(newLevel);
        }}
        style={{ margin: 5 }}
      >
        <option key={EASY} value={EASY}>Easy</option>
        <option key={MEDUIM} value={MEDUIM}>Meduim</option>
        <option key={HARD} value={HARD}>Hard</option>
      </select>
      <button onClick={() => setElements(generateSudoku(Number(level)))} className="action-buttons">New Game</button>
      <button onClick={handleSolveAll} className="action-buttons">Solve Game</button>
    </div>
  );
}
