import { compact } from 'lodash';


function getValue({ item, value, elements }) {
  return elements.filter((v) => v[item] === value && v);
}

export default function checkEntry({
  elements, column, row, box, index, value,
}) {
  const getHorizontal = getValue({
    item: 'column',
    value: column,
    elements,

  });
  const hor = compact(getHorizontal
    .filter((v) => v.index !== index && v).map((v) => v.value));
  const getVertical = getValue({ item: 'row', value: row, elements });
  const ver = compact(getVertical
    .filter((v) => v.index !== index && v).map((v) => v.value));
  const getBox = getValue({ item: 'box', value: box, elements });
  const bo = compact(getBox
    .filter((v) => v.index !== index && v).map((v) => v.value));
  const allPossibilities = [...hor, ...ver, ...bo];

  return !allPossibilities.includes(value);
}
