import React from 'react';
import {
  withState, withHandlers, compose,
} from 'recompose';
import { solvingBackgroundColor as backgroundColor, EASY } from './utils/constants';
import { generateSudoku, editValue, markAsFilled } from './utils/handlers';
import './App.css';
import SudokuBoard from './components/SudokuBoard';
import KeypadContainer from './components/KeypadContainer';

function Sudoku({
  elements, setElements, selectedItem, handleSolveAll,
  keypadClickHandler, level, setLevel, itemSelectHandler,
}) {
  return (
    <div className="App">
      <SudokuBoard
        elements={elements}
        selectedItem={selectedItem}
        itemSelectHandler={itemSelectHandler}
      />
      <KeypadContainer
        keypadClickHandler={keypadClickHandler}
        setElements={setElements}
        level={level}
        handleSolveAll={handleSolveAll}
        setLevel={setLevel}
      />
    </div>
  );
}


export default compose(
  withState('elements', 'setElements', generateSudoku(27)),
  withState('selectedItem', 'setSelectedItem', null),
  withState('level', 'setLevel', Number(EASY)),
  withHandlers({
    itemSelectHandler:
    ({ selectedItem, setSelectedItem }) => (index) => setSelectedItem(index === selectedItem
      ? null : index),
    keypadClickHandler:
    ({ selectedItem, elements, setElements }) => (v) => {
      if (selectedItem || selectedItem === 0) {
        const newElements = editValue({
          elements, item: 'value', value: v, index: Number(selectedItem),
        });

        const checkAndMark = markAsFilled({ elements: newElements, index: Number(selectedItem) });

        setElements(checkAndMark);
      }
    },
    handleSolveAll: ({ elements, setElements }) => () => {
      const solving = elements.map((v) => ({ ...v, value: v.correctValue, backgroundColor }));

      setElements(solving);
    },
  }),
)(Sudoku);
